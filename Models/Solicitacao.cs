using System;

namespace ControleEntrega.Models
{
    public class Solicitacao
    {
        public int SolicitacaoID { get; set; }
        public string Placa { get; set; }
        public DateTime Criacao { get; set; }
        public DateTime? Entrega { get; set; }
    }
}
