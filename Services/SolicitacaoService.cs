using System;
using System.Collections.Generic;
using System.Linq;
using ControleEntrega.Models;

namespace ControleEntrega.Services
{
    public class SolicitacaoService
    {
        private readonly Random _random = new Random();
        private readonly List<Solicitacao> _solicitacoes = new List<Solicitacao>();
        private int _id = 1;

        public SolicitacaoService()
        {
            GerarSolicitacaoAleatoria(5);
        }

        public IEnumerable<Solicitacao> SolicitacoesPendentes()
        {
            return _solicitacoes.Where(x => x.Entrega == null);
        }

        public IEnumerable<Solicitacao> SolicitacoesEntregues()
        {
            return _solicitacoes.Where(x => x.Entrega != null);
        }

        public void ConfirmarEntrega(int solicitacaoID)
        {
            var solicitacao = _solicitacoes.FirstOrDefault(x => x.SolicitacaoID == solicitacaoID);

            if (solicitacao == null)
            {
                throw new Exception("Solicitação não encontrada");
            }

            if (solicitacao.Entrega != null)
            {
                throw new Exception("Solicitação já foi entregue");
            }

            solicitacao.Entrega = DateTime.UtcNow;
        }

        public void LimparHistorico()
        {
            for (var i = _solicitacoes.Count - 1; i >= 0; i--)
            {
                if (_solicitacoes[i].Entrega != null)
                {
                    _solicitacoes.RemoveAt(i);
                }
            }
        }

        public void GerarSolicitacaoAleatoria(int quantidade)
        {
            if (quantidade < 1 || quantidade > 100)
            {
                throw new Exception("Quantidade inválida");
            }

            for (var i = 0; i < quantidade; i++)
            {
                _solicitacoes.Add(new Solicitacao
                {
                    SolicitacaoID = _id,
                    Placa = GerarPlaca(),
                    Criacao = DateTime.UtcNow
                });

                _id++;
            }
        }

        private string GerarPlaca()
        {
            return
                char.ConvertFromUtf32(_random.Next('A', 'Z')) +
                char.ConvertFromUtf32(_random.Next('A', 'Z')) +
                char.ConvertFromUtf32(_random.Next('A', 'Z')) +
                char.ConvertFromUtf32(_random.Next('0', '9')) +
                char.ConvertFromUtf32(_random.Next('A', 'Z')) +
                char.ConvertFromUtf32(_random.Next('0', '9')) +
                char.ConvertFromUtf32(_random.Next('0', '9'));
        }
    }
}
