using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControleEntrega.Models;
using ControleEntrega.Services;
using Microsoft.AspNetCore.Mvc;

namespace ControleEntrega.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitacaoController : Controller
    {
        private readonly SolicitacaoService _solicitacaoService;

        public SolicitacaoController(SolicitacaoService solicitacaoService)
        {
            _solicitacaoService = solicitacaoService;
        }

        [HttpGet("pendentes")]
        public JsonResult Pendentes()
        {
            return Json(_solicitacaoService.SolicitacoesPendentes());
        }

        [HttpGet("entregues")]
        public JsonResult Entregues()
        {
            return Json(_solicitacaoService.SolicitacoesEntregues());
        }

        [HttpPost("confirmar-entrega")]
        public ActionResult ConfirmarEntrega([FromBody] ConfirmarEntregaDTO request)
        {
            _solicitacaoService.ConfirmarEntrega(request.SolicitacaoID);

            return Ok();
        }

        [HttpPost("limpar-historico")]
        public ActionResult LimparHistorico()
        {
            _solicitacaoService.LimparHistorico();

            return Ok();
        }

        [HttpPost("gerar-solicitacao-aleatoria")]
        public ActionResult GerarSolicitacaoAleatoria([FromBody] GerarSolicitacaoAleatoriaDTO request)
        {
            _solicitacaoService.GerarSolicitacaoAleatoria(request.Quantidade);

            return Ok();
        }
    }
}
